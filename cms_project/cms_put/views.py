from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Contenido


# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == 'PUT':
        if Contenido.objects.get(clave=llave) is None:
            valor = request.body.decode('utf-8')
            content = Contenido(clave=llave, valor=valor)
            content.save()
    try:
        respuesta = Contenido.objects.get(clave=llave).valor
    except Contenido.DoesNotExist:
        respuesta = '<html><body><h1>&iexcl;No existe contenido para la clave ' + llave + '!</h1></body></html>'
    return HttpResponse(respuesta)

